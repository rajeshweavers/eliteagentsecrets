<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Level;
use App\Models\Menu;
use App\Models\Notification;

class NotificationController extends Controller
{
    public function forgot_password() {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        $data['notification'] = Notification::where('type', 'Forgot Password')->first();
        return view('dashboard.admin.notification.forgotPassword', $data);
    }

    public function forgot_password_save(Request $request) {
        $request->validate([
            'subject' => 'required',
            'content' => 'required'
        ]);
        
        $notification = Notification::where('type', 'Forgot Password')->count();
        if ($notification) {
            $updating = Notification::where('type', 'Forgot Password')
                    ->update([
                        'subject' => $request->input('subject'),
                        'body' => $request->input('content')
                    ]);
        } else {
            $data = new Notification();
            $data->type = 'Forgot Password';
            $data->subject = $request->subject;
            $data->body = $request->content;
            $data->save();
        }

        return redirect()->route('admin.notification.forgot-password')->with('success', 'Data has been updated successfully');
    }
}
