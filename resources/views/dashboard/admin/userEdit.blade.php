@extends('dashboard.admin.layouts.master')
@section('title', 'Admin | User')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>User</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                <li class="breadcrumb-item active">Edit User</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                @if (Session::get('fail'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('fail') }}
                </div>
                @endif
              <!-- jquery validation -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Edit User</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('admin.users.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" value="{{ $user->name }}" disabled>
                            <span class="text-danger">@error('name'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" class="form-control" id="email" value="{{ $user->email }}" disabled>
                            <span class="text-danger">@error('email'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" class="form-control" id="phone" value="{{ $user->phone }}" disabled>
                            <span class="text-danger">@error('phone'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="bokerage">Brokerage</label>
                            <input type="text" name="bokerage" class="form-control" id="bokerage" value="{{ $user->brokerage }}" disabled>
                            <span class="text-danger">@error('bokerage'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="entered_code">Entered Code</label>
                            <input type="text" name="entered_code" class="form-control" id="entered_code" value="{{ $user->entered_code }}" disabled>
                            <span class="text-danger">@error('entered_code'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="reffer_code">Refer Code</label>
                            <input type="text" name="reffer_code" class="form-control" id="reffer_code" value="{{ $user->reffer_code }}" disabled>
                            <span class="text-danger">@error('reffer_code'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="total_referral">Total Referral</label>
                            <input type="text" name="total_referral" class="form-control" id="total_referral" value="{{ $user->total_referral }}">
                            <span class="text-danger">@error('total_referral'){{ $message }}@enderror</span>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
@endsection
