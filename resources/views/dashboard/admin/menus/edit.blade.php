@extends('dashboard.admin.layouts.master')
@section('title', 'Admin | Menus')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Menus</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                <li class="breadcrumb-item active">Edit Menu</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                @if (Session::get('fail'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('fail') }}
                </div>
                @endif
              <!-- jquery validation -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Edit Menu</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('admin.menus.update', $menu->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" id="title" value="{{ $menu->title }}">
                            <span class="text-danger">@error('title'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="orderby">Order By</label>
                            <input type="number" name="orderby" class="form-control" id="orderby" value="{{ $menu->order_by }}">
                            <span class="text-danger">@error('orderby'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="menu_type">Menu Type</label>
                            <select name="menu_type" id="menu_type" class="form-control">
                              <option value="" @if ($menu->menu_type == '') selected @endif disabled>Select</option>
                              <option value="url" @if ($menu->menu_type == 'url') selected @endif>URL</option>
                              <option value="page" @if ($menu->menu_type == 'page') selected @endif>Page</option>
                            </select>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="other_menu" name="other_menu" type="checkbox" value="1" @if ($menu->is_other_menu) checked @endif>
                            <label class="form-check-label" for="other_menu">Is Other Menu</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="isActive" name="isActive" type="checkbox" value="1" @if ($menu->status) checked @endif>
                            <label class="form-check-label" for="isActive">Is Active</label>
                        </div>
                        <div class="form-group">
                          <label for="menu_icon">Menu Icon</label>
                          <input type="file" name="icon" class="form-control" id="menu_icon" accept="image/png, image/jpeg">
                        </div>
                        <div class="form-group">
                          <input type="hidden" name="oldfilename" value="{{ $menu->icon }}">
                          @if ($menu->icon)
                            <img src="{{ asset('uploads/').'/'.$menu->icon }}" style="width:20px; background-color:black;">
                          @endif
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
@endsection
