@extends('dashboard.user.layouts.master')
@section('title', 'Tool')

@section('content')

<div class="current-ref sec_pad text-center">
    <h2>Refer-O-Meter - Current Referrals</h2>
    <ul class="d-flex">
        @foreach ($levels as $level)
            <li @if ($max_level->id < $level->id)class="disabled" @endif>
                <span class="img-bg"><img src="{{ asset('uploads/').'/'.$level->icon }}" /></span>
                <strong class="d-block">{{ $level->title }} <b>{{ $level->referrals }}</b></strong>
            </li>
        @endforeach
    </ul>
</div>
<div class="awesome-vidz">
    <div id="accordion" class="faq-sec">
        @foreach ($tools as $tool)
            <div class="card">
                <div class="card-header" id="heading{{ $loop->index }}">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse{{ $loop->index }}" aria-expanded="false" aria-controls="collapse{{ $loop->index }}">
                            {{ $tool->question }}
                        </button>
                    </h5>
                </div>
                <div id="collapse{{ $loop->index }}" class="collapse" aria-labelledby="heading{{ $loop->index }}" data-parent="#accordion">
                    <div class="card-body">
                        {!! $tool->answer !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection
