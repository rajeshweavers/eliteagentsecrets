<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Models\Menu;
use App\Models\Level;
use App\Models\Tool;

class ToolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        $data['tools'] = Tool::all();
        return view('dashboard.admin.tool.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.tool.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'question' => 'required',
            'answer' => 'required'
        ]);

        $data = new Tool();
        $data->question = $request->question;
        $data->answer = $request->answer;

        $save = $data->save();

        if($save) {
            return redirect()->route('admin.tool')->with('success', 'Data has been added successfully');
        } else {
            return redirect()->back()->with('fail', 'Failed to add data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tool'] = Tool::find($id);
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.tool.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'question' => 'required',
            'answer' => 'required'
        ]);

        $updating = Tool::where('id', $id)
                    ->update([
                        'question' => $request->input('question'),
                        'answer' => $request->input('answer')
                    ]);
        return redirect()->route('admin.tool')->with('success', 'Data has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleting = Tool::destroy($id);
        return redirect()->route('admin.tool')->with('success', 'Data has been deleted successfully');
    }
}
