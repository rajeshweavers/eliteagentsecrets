@extends('dashboard.admin.layouts.master')
@section('title', 'Admin | Menus')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Menus</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                <li class="breadcrumb-item active">Menus</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
                <div class="col-lg-6">
                    @if (Session::get('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('success') }}
                    </div>
                    @endif
                    <!-- checkbox -->
                    <div class="card card-primary">
                        <div class="card-header">
                          <h3 class="card-title">Menus</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('admin.menus.save') }}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" name="dashboard" type="checkbox" value="1" checked disabled>
                                        <label class="form-check-label">Dashboard</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" id="levels" name="levels" type="checkbox" value="1" @if ($menus[1]->status) checked @endif>
                                        <label class="form-check-label" for="levels">Levels</label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" id="refer" name="refer" type="checkbox" value="1" @if ($menus[2]->status) checked @endif>
                                        <label class="form-check-label" for="refer">Refer People</label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" id="faq" name="faq" type="checkbox" value="1" @if ($menus[3]->status) checked @endif>
                                        <label class="form-check-label" for="faq">FAQ</label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" id="webinar" name="webinar" type="checkbox" value="1" @if ($menus[4]->status) checked @endif>
                                        <label class="form-check-label" for="webinar">Bonus Webinar</label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" id="facebook" name="facebook" type="checkbox" value="1" @if ($menus[5]->status) checked @endif>
                                        <label class="form-check-label" for="facebook">Facebook Group</label>
                                      </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                      </div>
                  </div>

          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
@endsection
