<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Models\Menu;
use App\Models\Level;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['menus'] = Menu::orderBy('order_by')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        return view('dashboard.admin.menus.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.menus.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'orderby' => 'required'
        ]);

        // upload image
        $path = 'uploads/';
        $newname = Helper::renameImageFile($path, $request->file('icon')->getClientOriginalName());
        $upload = $request->icon->move(public_path($path), $newname);

        $isActive = $request->input('isActive') ? $request->input('isActive') : 0;
        $isOtherMenu = $request->input('other_menu') ? $request->input('other_menu') : 0;
        $link = Helper::generateLink($request->title);

        $data = new Menu();
        $data->title = $request->title;
        $data->order_by = $request->orderby;
        $data->link = $link;
        $data->is_other_menu = $isOtherMenu;
        $data->menu_type = $request->menu_type;
        $data->status = $isActive;
        $data->icon = $newname;
        $save = $data->save();

        if($save) {
            return redirect()->route('admin.menus')->with('success', 'Data has been added successfully');
        } else {
            return redirect()->back()->with('fail', 'Failed to add data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['menu'] = Menu::find($id);
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.menus.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'orderby' => 'required'
        ]);

        $newname = '';

        if ($request->hasFile('icon')) {
            // upload image
            $path = 'uploads/';
            $newname = Helper::renameImageFile($path, $request->file('icon')->getClientOriginalName());
            $upload = $request->icon->move(public_path($path), $newname);
        }

        if ($newname) {
            $updatedName = $newname;
        } else {
            $updatedName = $request->input('oldfilename');
        }

        $isActive = $request->input('isActive') ? $request->input('isActive') : 0;
        $isOtherMenu = $request->input('other_menu') ? $request->input('other_menu') : 0;

        $updating = Menu::where('id', $id)
                    ->update([
                        'title' => $request->input('title'),
                        'order_by' => $request->input('orderby'),
                        'is_other_menu' => $isOtherMenu,
                        'menu_type' => $request->input('menu_type'),
                        'status' => $isActive,
                        'icon' => $updatedName
                    ]);
        return redirect()->route('admin.menus')->with('success', 'Data has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleting = Menu::destroy($id);
        return redirect()->route('admin.menus')->with('success', 'Data has been deleted successfully');
    }
}
