@extends('dashboard.admin.layouts.master')
@section('title', 'Admin | Webhook | Outgoing')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Outgoing Webhook</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                <li class="breadcrumb-item active">Edit Webhook</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                @if (Session::get('fail'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('fail') }}
                </div>
                @endif
              <!-- jquery validation -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Edit Webhook</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('admin.webhook.outgoing.update', $webhook->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" id="title" value="{{ $webhook->title }}">
                            <span class="text-danger">@error('title'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control" id="description" rows="4">@if ($webhook) {{ $webhook->description }} @endif</textarea>
                            <span class="text-danger">@error('description'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="url">Webhook Url</label>
                            <input type="text" name="url" class="form-control" id="url" value="{{ $webhook->webhook_url }}">
                            <span class="text-danger">@error('url'){{ $message }}@enderror</span>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Webhook Trigger</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="register" name="trigger[]" type="checkbox" value="user_add" @if (strpos($webhook->trigger, 'user_add') !== false) checked @endif>
                            <label class="form-check-label" for="register">User Register</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="update" name="trigger[]" type="checkbox" value="user_password_update" @if (strpos($webhook->trigger, 'user_password_update') !== false) checked @endif>
                            <label class="form-check-label" for="update">Password Update</label>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Fields Type</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="name" name="fields[]" type="checkbox" value="user_name" @if (strpos($webhook->fields, 'user_name') !== false) checked @endif>
                            <label class="form-check-label" for="name">Name</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="email" name="fields[]" type="checkbox" value="user_email" @if (strpos($webhook->fields, 'user_email') !== false) checked @endif>
                            <label class="form-check-label" for="email">Email</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="phone" name="fields[]" type="checkbox" value="user_phone" @if (strpos($webhook->fields, 'user_phone') !== false) checked @endif>
                            <label class="form-check-label" for="phone">Phone</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="brokerage" name="fields[]" type="checkbox" value="user_brokerage" @if (strpos($webhook->fields, 'user_brokerage') !== false) checked @endif>
                            <label class="form-check-label" for="brokerage">Brokerage</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="password" name="fields[]" type="checkbox" value="user_password" @if (strpos($webhook->fields, 'user_password') !== false) checked @endif>
                            <label class="form-check-label" for="password">Password</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="entered_code" name="fields[]" type="checkbox" value="user_entered_code" @if (strpos($webhook->fields, 'user_entered_code') !== false) checked @endif>
                            <label class="form-check-label" for="entered_code">Entered Code</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="reffer_code" name="fields[]" type="checkbox" value="user_refer_code" @if (strpos($webhook->fields, 'user_refer_code') !== false) checked @endif>
                            <label class="form-check-label" for="reffer_code">Reffer Code</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="total_referral" name="fields[]" type="checkbox" value="user_total_referral" @if (strpos($webhook->fields, 'user_total_referral') !== false) checked @endif>
                            <label class="form-check-label" for="total_referral">Total Referral</label>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
@endsection
