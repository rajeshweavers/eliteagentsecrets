<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Menu;
use App\Models\Level;
use App\Models\Page;

class PageController extends Controller
{
    function index($id) {
        $data['page'] = Menu::find($id);
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['page_content'] = Page::where('menu_id', $id)->first();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.pages.index', $data);
    }

    function update(Request $request, $id) {
        $request->validate([
            'title' => 'required'
        ]);

        $page = Page::where('menu_id', $id)->first();
        
        if ($page === null) {
            $data = new Page();
            $data->menu_id = $id;
            $data->title = $request->input('title');
            $data->content = $request->input('content');
            $data->save();
        } else {
            $updating = Page::where('id', $page->id)
                    ->update([
                        'title' => $request->input('title'),
                        'content' => $request->input('content')
                    ]);
        }
        
        return redirect()->route('admin.pages', $id)->with('success', 'Data has been updated successfully');
    }
}
