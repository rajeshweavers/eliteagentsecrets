<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Models\Menu;
use App\Models\Level;
use App\Models\Tool;
use App\Models\Webhook;
use App\Models\User;

class WebhookController extends Controller
{
    public function outgoing() {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['outgoing_webhooks'] = Webhook::where('type', 'outgoing')->orderByDesc('id')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.webhook.outgoing.index', $data);
    }

    public function outgoing_create() {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.webhook.outgoing.add', $data);
    }

    public function outgoing_store(Request $request) {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
        ]);

        $trigger_points = implode(',', $request->trigger);
        $fields_name = implode(',', $request->fields);

        $data = new Webhook();
        $data->title = $request->title;
        $data->description = $request->description;
        $data->webhook_url = $request->url;
        $data->trigger = $trigger_points;
        $data->fields = $fields_name;
        $data->type = 'outgoing';
        $save = $data->save();

        if ($save) {
            return redirect()->route('admin.webhook.outgoing')->with('success', 'Data has been added successfully');
        } else {
            return redirect()->back()->with('fail', 'Failed to add data');
        }
    }

    function outgoing_all_user() {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['users'] = User::orderByDesc('id')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.webhook.outgoing.index', $data);
    }

    function outgoing_edit($id) {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        $data['webhook'] = Webhook::find($id);
        return view('dashboard.admin.webhook.outgoing.edit', $data);
    }

    function outgoing_update(Request $request, $id) {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
        ]);

        $trigger_points = implode(',', $request->trigger);
        $fields_name = implode(',', $request->fields);

        $updating = Webhook::where('id', $id)
                    ->update([
                        'title' => $request->input('title'),
                        'description' => $request->input('description'),
                        'webhook_url' => $request->input('url'),
                        'trigger' => $trigger_points,
                        'fields' => $fields_name,
                    ]);
        return redirect()->route('admin.webhook.outgoing')->with('success', 'Data has been updated successfully');
    }

    function outgoing_delete($id) {
        $deleting = Webhook::destroy($id);
        return redirect()->route('admin.webhook.outgoing')->with('success', 'Data has been deleted successfully');
    }

    public function incoming() {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['incoming_webhooks'] = Webhook::where('type', 'incoming')->orderByDesc('id')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.webhook.incoming.index', $data);
    }

    public function incoming_create() {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.webhook.incoming.add', $data);
    }

    public function incoming_store(Request $request) {
        $request->validate([
            'title' => 'required',
        ]);

        $base_url = env('APP_URL');

        $fields_name = implode(',', $request->fields);

        $webhook_url = $base_url.'/incoming-webhook';

        $data = new Webhook();
        $data->title = $request->title;
        $data->description = $request->description;
        $data->webhook_url = $webhook_url;
        $data->fields = $fields_name;
        $data->type = 'incoming';
        $save = $data->save();

        if ($save) {
            return redirect()->route('admin.webhook.incoming')->with('success', 'Data has been added successfully');
        } else {
            return redirect()->back()->with('fail', 'Failed to add data');
        }
    }

    public function incoming_edit($id) {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        $data['webhook'] = Webhook::find($id);
        return view('dashboard.admin.webhook.incoming.edit', $data);
    }

    public function incoming_update(Request $request, $id) {
        $request->validate([
            'title' => 'required',
        ]);

        $fields_name = implode(',', $request->fields);

        $updating = Webhook::where('id', $id)
                    ->update([
                        'title' => $request->input('title'),
                        'description' => $request->input('description'),
                        'fields' => $fields_name,
                    ]);
        return redirect()->route('admin.webhook.incoming')->with('success', 'Data has been updated successfully');
    }

    public function incoming_delete($id) {
        $deleting = Webhook::destroy($id);
        return redirect()->route('admin.webhook.incoming')->with('success', 'Data has been deleted successfully');
    }

    public function store_user(Request $request) {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->brokerage = $request->brokerage;
        $user->entered_code = $request->entered_code;
        $user->reffer_code = $request->refer_code;
        $user->total_referral = $request->total_referral;
        $user->password = \Hash::make($request->password);
        $save = $user->save();

        return response()->noContent();
    }
}
