@extends('dashboard.admin.layouts.master')
@section('title', 'Admin | Webhook | Incoming')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Incoming Webhook</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                <li class="breadcrumb-item active">Add Webhook</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                @if (Session::get('fail'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('fail') }}
                </div>
                @endif
              <!-- jquery validation -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Add Webhook</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('admin.webhook.incoming.save') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" id="title" value="{{ old('title') }}">
                            <span class="text-danger">@error('title'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control" id="description" rows="4"></textarea>
                            <span class="text-danger">@error('description'){{ $message }}@enderror</span>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Fields Type</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="name" name="fields[]" type="checkbox" value="user_name">
                            <label class="form-check-label" for="name">Name</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="email" name="fields[]" type="checkbox" value="user_email">
                            <label class="form-check-label" for="email">Email</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="phone" name="fields[]" type="checkbox" value="user_phone">
                            <label class="form-check-label" for="phone">Phone</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="brokerage" name="fields[]" type="checkbox" value="user_brokerage">
                            <label class="form-check-label" for="brokerage">Brokerage</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="password" name="fields[]" type="checkbox" value="user_password">
                            <label class="form-check-label" for="password">Password</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="entered_code" name="fields[]" type="checkbox" value="user_entered_code">
                            <label class="form-check-label" for="entered_code">Entered Code</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="reffer_code" name="fields[]" type="checkbox" value="user_refer_code">
                            <label class="form-check-label" for="reffer_code">Reffer Code</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="total_referral" name="fields[]" type="checkbox" value="user_total_referral">
                            <label class="form-check-label" for="total_referral">Total Referral</label>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
@endsection
