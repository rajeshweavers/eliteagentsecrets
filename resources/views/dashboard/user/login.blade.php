<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Login</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('user/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{ asset('user/css/custom.css') }}" />
    </head>
    <body class="login-template">
        <div class="registration-bg for-login-sec sec_pad give_code_form">
            <div class="login-btn-sec text-right">
                <a href="#" class="login-btn" data-toggle="modal" data-target="#loginModal">Login</a>
            </div>
            <div class="reg-wrap text-center">
                <div class="logo-sec">
                    <a href="#"><img src="{{ asset('user/images/logo2.png') }}" /></a>
                    <p>{{ $content->text_one }}</p>
                </div>

                <div id="keypad" class="keypad-bg">
                    <div class="login-text-wrap text-center">
                        <strong class="d-block">{{ $content->text_two }}</strong>
                        <p>{{ $content->text_three }}</p>
                    </div>
                    <div id="panel" class="for-number">
                        <p class="innahpanel">000000</p>
                        <div class="error_code" style="color: #d81010;"></div>
                        <input type="hidden" value="000000" class="panel_value">
                        <input type="hidden" value="" class="new_value">
                        <input type="hidden" value="0" class="exact_value">
                    </div>
                    <div class="button-row">
                        <div class="button" id="b1" data-value="1">1<span>&nbsp;</span></div>
                        <div class="button" id="b2" data-value="2">2<span>ABC</span></div>
                        <div class="button" id="b3" data-value="3">3<span>DEF</span></div>
                    </div>
                    <div class="button-row">
                        <div class="button" id="b4" data-value="4">4<span>GHI</span></div>
                        <div class="button" id="b5" data-value="5">5<span>JKL</span></div>
                        <div class="button" id="b6" data-value="6">6<span>MNO</span></div>
                    </div>
                    <div class="button-row">
                        <div class="button" id="b7" data-value="7">7<span>PQRS</span></div>
                        <div class="button" id="b8" data-value="8">8<span>TUV</span></div>
                        <div class="button" id="b9" data-value="9">9<span>WXYZ</span></div>
                    </div>
                    <div class="button-row">
                        <div class="button" id="bx" data-value="x"><img src="{{ asset('user/images/left-arw.png') }}" /><span>CLEAR</span></div>
                        <div class="button" id="b0" data-value="0">0<span>+</span></div>
                        <div class="button" id="bg" data-value="g"><img src="{{ asset('user/images/key.png') }}" /><span>GO!</span></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Login Modal -->
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Login Details</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    @if (Session::get('fail'))
                        <div class="text-danger">{{ Session::get('fail') }}</div>
                    @endif
                    <form action="{{ route('user.check') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('email') }}">
                            <small class="form-text text-danger">@error('email') {{ $message }} @enderror</small>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                            <small class="form-text text-danger">@error('password') {{ $message }} @enderror</small>
                        </div>
                        <div class="form_footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <span class="for_got"><a href="#">Forgotten password?</a></span>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>

        <!-- Forgot Password Modal -->
        <div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Forgotten Password</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    @if (Session::get('fail'))
                        <div class="text-danger">{{ Session::get('fail') }}</div>
                    @endif
                    <div class="enter_email_sec">
                        <form action="" method="POST" id="submit_forget_email">
                            @csrf
                            <div class="form-group">
                                <label for="forgot_email">We will send you a one-time password to your registered email to verify its you</label>
                                <input type="email" class="form-control" name="forgot_email" aria-describedby="emailHelp" placeholder="Enter email" id="forgot_email" value="{{ old('forgot_email') }}">
                                <small class="form-text text-danger forgot_email_error"></small>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    <div class="enter_otp_sec" style="display: none;">
                        <form action="" method="POST" id="submit_forget_otp">
                            @csrf
                            <div class="form-group">
                                <label for="forgot_otp">Enter One-time Password</label>
                                <input type="text" class="form-control" name="forgot_otp" aria-describedby="emailHelp" placeholder="Enter OTP" id="forgot_otp" value="{{ old('forgot_otp') }}">
                                <small class="form-text text-danger forgot_otp_error"></small>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    <div class="enter_pass_sec" style="display: none;">
                        <form action="" method="POST" id="submit_new_pass">
                            @csrf
                            <div class="form-group">
                                <input type="password" class="form-control" name="new_password" placeholder="Enter Password" id="new_password" value="{{ old('new_password') }}">
                                <small class="form-text text-danger forgot_new_pass_error"></small>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" id="confirm_password" value="{{ old('confirm_password') }}">
                                <small class="form-text text-danger forgot_pass_error"></small>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
              </div>
            </div>
        </div>

        <script src="{{ asset('user/js/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('user/js/propper.js') }}"></script>
        <script src="{{ asset('user/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('user/js/custom.js') }}"></script>

        <script>
            $('#submit_forget_email').on('submit', function(e) {
                e.preventDefault();
                var forgot_email = $('#forgot_email').val();
                if(forgot_email) {
                    var valid_email = checkValidEmail(forgot_email);
                    if(valid_email) {
                        checkEmailId(forgot_email);
                    } else {
                        $('.forgot_email_error').html('Please enter a valid email.');
                    }
                } else {
                    $('.forgot_email_error').html('This field is required.');
                }
            });

            function checkValidEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            }

            function checkEmailId(email) {
                var datas = {"_token": "{{ csrf_token() }}", "email": email};
                $.get('/user/check-email/', datas, function(result) {
                    if(result === 'found') {
                        $('.enter_email_sec').hide();
                        $('.enter_otp_sec').show();
                        sessionStorage.setItem('forgot_email', email);
                    } else {
                        $('.forgot_email_error').html('Email ID did not match.');
                    }
                });
            }

            $('#submit_forget_otp').on('submit', function(e) {
                e.preventDefault();
                var forgot_otp = $('#forgot_otp').val();
                if(forgot_otp) {
                    var email = sessionStorage.getItem('forgot_email');
                    var datas = {"_token": "{{ csrf_token() }}", "otp": forgot_otp, "email": email};
                    $.get('/user/check-otp/', datas, function(result) {
                        if(result === 'found') {
                            $('.enter_email_sec').hide();
                            $('.enter_otp_sec').hide();
                            $('.enter_pass_sec').show();
                        }else if(result === 'expired') {
                            $('.forgot_otp_error').html('OTP has been expired.');
                        } else {
                            $('.forgot_otp_error').html('OTP did not match.');
                        }
                    });
                } else {
                    $('.forgot_otp_error').html('This field is required.');
                }
            });

            $('#submit_new_pass').on('submit', function(e) {
                e.preventDefault();
                var new_password = $('#new_password').val();
                var confirm_password = $('#confirm_password').val();
                if (!new_password) {
                    $('.forgot_new_pass_error').html('This field is required.');
                }
                if (!confirm_password) {
                    $('.forgot_pass_error').html('This field is required.');
                }
                if (new_password === confirm_password) {
                    var email = sessionStorage.getItem('forgot_email');
                    var datas = {"_token": "{{ csrf_token() }}", "new_password": new_password, "email": email};
                    $.get('/user/update-new-password/', datas, function(result) {
                        if(result === 'complete') {
                            $('#forgotPasswordModal').modal('toggle');
                            $('#loginModal').modal('toggle');
                        } else {
                            $('.forgot_pass_error').html('Something went wrong. Please try again.');
                        }
                    });
                } else {
                    $('.forgot_pass_error').html('Confirm password did not match.');
                }
            });
        </script>
    </body>
</html>
