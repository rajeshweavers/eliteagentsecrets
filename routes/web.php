<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\LevelController;
use App\Http\Controllers\Admin\ContentController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\ToolController;
use App\Http\Controllers\Admin\WebhookController;
use App\Http\Controllers\Admin\NotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('dashboard.user.login');
// });

Route::post('/incoming-webhook', [WebhookController::class, 'store_user'])->name('webhook.store-user');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// User Routes
Route::middleware(['guest:web', 'PreventBackHistory:web'])->group(function() {

    Route::get('/', [UserController::class, 'login'])->name('user.index');
    Route::get('/user/login', [UserController::class, 'login'])->name('user.login');
    Route::get('/user/register', [UserController::class, 'register'])->name('user.register');
    Route::post('/user/create', [UserController::class, 'create'])->name('user.create');
    Route::post('/user/check', [UserController::class, 'check'])->name('user.check');
    Route::get('/user/refer-check/{id}', [UserController::class, 'refer_check'])->name('refer.check');
    Route::get('/user/check-email', [UserController::class, 'check_email'])->name('user.checkEmail');
    Route::get('/user/check-otp', [UserController::class, 'check_otp'])->name('user.checkotp');
    Route::get('/user/update-new-password', [UserController::class, 'update_new_password'])->name('user.update-new-password');
});

Route::middleware(['auth:web', 'PreventBackHistory:web'])->group(function() {

    Route::get('/user/home', [UserController::class, 'index'])->name('user.home');
    Route::post('/user/logout', [UserController::class, 'logout'])->name('user.logout');
    Route::get('/user/level/{id}', [UserController::class, 'level'])->name('user.level');
    Route::get('/user/refer', [UserController::class, 'refer'])->name('user.refer');
    Route::get('/user/faq', [UserController::class, 'faq'])->name('user.faq');
    Route::get('/user/pages/{id}', [UserController::class, 'page'])->name('user.page');
    Route::get('/user/tool', [UserController::class, 'tool'])->name('user.tool');
});

// Admin routes
Route::middleware(['guest:admin', 'PreventBackHistory:admin'])->group(function() {

    Route::get('/admin', [AdminController::class, 'index'])->name('admin.index');
    Route::view('/admin/login', 'dashboard.admin.login')->name('admin.login');
    Route::post('/admin/check', [AdminController::class, 'check'])->name('admin.check');
});

Route::middleware(['auth:admin', 'PreventBackHistory:admin'])->group(function() {

    Route::get('/admin/home', [AdminController::class, 'home'])->name('admin.home');
    Route::post('/admin/logout', [AdminController::class, 'logout'])->name('admin.logout');

    Route::get('/admin/users', [AdminController::class, 'users'])->name('admin.users');
    Route::get('/admin/users/edit/{id}', [AdminController::class, 'user_edit'])->name('admin.users.edit');
    Route::post('/admin/users/update/{id}', [AdminController::class, 'user_update'])->name('admin.users.update');

    Route::get('/admin/contents/dashboard', [ContentController::class, 'dashboard'])->name('admin.contents.dashboard');
    Route::post('/admin/contents/dashboard-save', [ContentController::class, 'dashboard_save'])->name('admin.contents.dashboard.save');

    Route::get('/admin/contents/login', [ContentController::class, 'login'])->name('admin.contents.login');
    Route::post('/admin/contents/login-save', [ContentController::class, 'login_save'])->name('admin.contents.login.save');

    Route::get('/admin/contents/register', [ContentController::class, 'register'])->name('admin.contents.register');
    Route::post('/admin/contents/register-save', [ContentController::class, 'register_save'])->name('admin.contents.register.save');
    Route::get('/admin/contents/register-add', [ContentController::class, 'register_add'])->name('admin.contents.register.add');
    Route::post('/admin/contents/register-add-save', [ContentController::class, 'register_add_save'])->name('admin.contents.register.add.save');
    Route::get('/admin/contents/register-edit/{id}', [ContentController::class, 'register_edit'])->name('admin.contents.register.edit');
    Route::post('/admin/contents/register-edit-save', [ContentController::class, 'register_edit_save'])->name('admin.contents.register.edit.save');
    Route::get('/admin/contents/register-delete/{id}', [ContentController::class, 'register_delete'])->name('admin.contents.register.delete');

    Route::get('/admin/contents/facebook', [ContentController::class, 'facebook'])->name('admin.contents.facebook');
    Route::post('/admin/contents/facebook-save', [ContentController::class, 'facebook_save'])->name('admin.contents.facebook.save');

    Route::get('/admin/contents/webinar', [ContentController::class, 'webinar'])->name('admin.contents.webinar');
    Route::post('/admin/contents/webinar-save', [ContentController::class, 'webinar_save'])->name('admin.contents.webinar.save');

    Route::get('/admin/contents/refer', [ContentController::class, 'refer'])->name('admin.contents.refer');
    Route::post('/admin/contents/refer-save', [ContentController::class, 'refer_save'])->name('admin.contents.refer.save');

    Route::get('/admin/contents/url/{id}', [ContentController::class, 'url_content'])->name('admin.contents.url');
    Route::post('/admin/contents/url-save/{id}', [ContentController::class, 'url_content_save'])->name('admin.contents.url.save');

    Route::get('/admin/levels', [LevelController::class, 'index'])->name('admin.levels');
    Route::get('/admin/levels/add', [LevelController::class, 'add'])->name('admin.levels.add');
    Route::post('/admin/levels/save', [LevelController::class, 'save'])->name('admin.levels.save');
    Route::get('/admin/levels/edit/{id}', [LevelController::class, 'edit'])->name('admin.levels.edit');
    Route::post('/admin/levels/update', [LevelController::class, 'update'])->name('admin.levels.update');
    Route::get('/admin/levels/delete/{id}', [LevelController::class, 'delete'])->name('admin.levels.delete');

    Route::get('/admin/level/{id}', [LevelController::class, 'level_content'])->name('admin.level');
    Route::get('/admin/level/add/{id}', [LevelController::class, 'level_content_add'])->name('admin.level.add');
    Route::post('/admin/level/save', [LevelController::class, 'level_content_save'])->name('admin.level.save');
    Route::get('/admin/level/edit/{lid}/{id}', [LevelController::class, 'level_content_edit'])->name('admin.level.edit');
    Route::post('/admin/level/update', [LevelController::class, 'level_content_update'])->name('admin.level.update');
    Route::get('/admin/level/delete/{lid}/{id}', [LevelController::class, 'level_content_delete'])->name('admin.level.delete');

    Route::get('/admin/contents/faqs', [FaqController::class, 'index'])->name('admin.contents.faqs');
    Route::get('/admin/contents/faqs/add', [FaqController::class, 'create'])->name('admin.contents.faqs.add');
    Route::post('/admin/contents/faqs/save', [FaqController::class, 'store'])->name('admin.contents.faqs.save');
    Route::get('/admin/contents/faqs/edit/{id}', [FaqController::class, 'edit'])->name('admin.contents.faqs.edit');
    Route::post('/admin/contents/faqs/update/{id}', [FaqController::class, 'update'])->name('admin.contents.faqs.update');
    Route::get('/admin/contents/faqs/delete/{id}', [FaqController::class, 'destroy'])->name('admin.contents.faqs.delete');
    Route::post('/admin/contents/faqs/upload', [FaqController::class, 'upload'])->name('admin.contents.faqs.upload');

    Route::get('/admin/menus', [MenuController::class, 'index'])->name('admin.menus');
    Route::get('/admin/menus/add', [MenuController::class, 'create'])->name('admin.menus.add');
    Route::post('/admin/menus/save', [MenuController::class, 'store'])->name('admin.menus.save');
    Route::get('/admin/menus/edit/{id}', [MenuController::class, 'edit'])->name('admin.menus.edit');
    Route::post('/admin/menus/update/{id}', [MenuController::class, 'update'])->name('admin.menus.update');
    Route::get('/admin/menus/delete/{id}', [MenuController::class, 'destroy'])->name('admin.menus.delete');
    Route::post('/admin/menus/upload', [MenuController::class, 'upload'])->name('admin.menus.upload');

    Route::get('/admin/pages/{id}', [PageController::class, 'index'])->name('admin.pages');
    Route::post('/admin/pages/update/{id}', [PageController::class, 'update'])->name('admin.pages.update');

    Route::get('/admin/tool', [ToolController::class, 'index'])->name('admin.tool');
    Route::get('/admin/tool/add', [ToolController::class, 'create'])->name('admin.tool.add');
    Route::post('/admin/tool/save', [ToolController::class, 'store'])->name('admin.tool.save');
    Route::get('/admin/tool/edit/{id}', [ToolController::class, 'edit'])->name('admin.tool.edit');
    Route::post('/admin/tool/update/{id}', [ToolController::class, 'update'])->name('admin.tool.update');
    Route::get('/admin/tool/delete/{id}', [ToolController::class, 'destroy'])->name('admin.tool.delete');

    Route::get('/admin/webhook/outgoing', [WebhookController::class, 'outgoing'])->name('admin.webhook.outgoing');
    Route::get('/admin/webhook/outgoing/add', [WebhookController::class, 'outgoing_create'])->name('admin.webhook.outgoing.add');
    Route::post('/admin/webhook/outgoing/save', [WebhookController::class, 'outgoing_store'])->name('admin.webhook.outgoing.save');
    Route::get('/admin/webhook/outgoing/edit/{id}', [WebhookController::class, 'outgoing_edit'])->name('admin.webhook.outgoing.edit');
    Route::post('/admin/webhook/outgoing/update/{id}', [WebhookController::class, 'outgoing_update'])->name('admin.webhook.outgoing.update');
    Route::get('/admin/webhook/outgoing/delete/{id}', [WebhookController::class, 'outgoing_delete'])->name('admin.webhook.outgoing.delete');

    Route::get('/admin/webhook/incoming', [WebhookController::class, 'incoming'])->name('admin.webhook.incoming');
    Route::get('/admin/webhook/incoming/add', [WebhookController::class, 'incoming_create'])->name('admin.webhook.incoming.add');
    Route::post('/admin/webhook/incoming/save', [WebhookController::class, 'incoming_store'])->name('admin.webhook.incoming.save');
    Route::get('/admin/webhook/incoming/edit/{id}', [WebhookController::class, 'incoming_edit'])->name('admin.webhook.incoming.edit');
    Route::post('/admin/webhook/incoming/update/{id}', [WebhookController::class, 'incoming_update'])->name('admin.webhook.incoming.update');
    Route::get('/admin/webhook/incoming/delete/{id}', [WebhookController::class, 'incoming_delete'])->name('admin.webhook.incoming.delete');

    Route::get('/admin/notification/forgot-password', [NotificationController::class, 'forgot_password'])->name('admin.notification.forgot-password');
    Route::post('/admin/notification/forgot-password/save', [NotificationController::class, 'forgot_password_save'])->name('admin.notification.forgot-password.save');
});

Route::post('/incoming-webhook', [WebhookController::class, 'incoming_store'])->name('admin.webhook.incoming.save');
