<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Level;
use App\Models\User;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    function index() {
        return view('dashboard.admin.login');
    }

    function home() {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['total_users'] = User::count();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.home', $data);
    }

    function check(Request $request) {
        // validate inputs
        $request->validate([
            'username' => 'required|exists:admins,username',
            'password' => 'required|min:5|max:20'
        ],[
            'username.exists' => 'This username is not exists.'
        ]);

        $creds = $request->only('username', 'password');
        if (Auth::guard('admin')->attempt($creds)) {
            return redirect()->route('admin.home');
        } else {
            return redirect()->route('admin.login')->with('fail', 'Incorrect credentials');
        }
    }

    function logout() {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

    function users() {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['users'] = User::orderByDesc('id')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.users', $data);
    }

    function menus() {
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['menus'] = Menu::all();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.menus', $data);
    }

    function menus_save(Request $request) {
        $dashboard = $request->input('dashboard') ? $request->input('dashboard') : 0;
        $levels = $request->input('levels') ? $request->input('levels') : 0;
        $refer = $request->input('refer') ? $request->input('refer') : 0;
        $faq = $request->input('faq') ? $request->input('faq') : 0;
        $webinar = $request->input('webinar') ? $request->input('webinar') : 0;
        $facebook = $request->input('facebook') ? $request->input('facebook') : 0;

        Menu::where('id', 2)->update(['status' => $levels]);
        Menu::where('id', 3)->update(['status' => $refer]);
        Menu::where('id', 4)->update(['status' => $faq]);
        Menu::where('id', 5)->update(['status' => $webinar]);
        Menu::where('id', 6)->update(['status' => $facebook]);

        return redirect()->route('admin.menus')->with('success', 'Data has been updated successfully');
    }

    function user_edit($id) {
        $data['user'] = User::find($id);
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['pages'] = Menu::where('menu_type', 'page')->get();
        $data['menus_item'] = Menu::where('menu_type', NULL)->where('link', '!=', 'levels')->orWhere('menu_type', 'url')->orderBy('order_by')->get();
        return view('dashboard.admin.userEdit', $data);
    }

    function user_update(Request $request, $id) {
        $updating = User::where('id', $id)
                    ->update([
                        'total_referral' => $request->input('total_referral')
                    ]);
        return redirect()->route('admin.users')->with('success', 'Data has been updated successfully');
    }
}
