<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>@yield('title')</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('user/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{ asset('user/css/custom.css') }}" />
    </head>
    <body>
        @php
            $menus = DB::table('menus')->orderBy('order_by')->get();
        @endphp
        <section class="menu-bar-outer" id="menu_target">
            <div class="toggle-btn text-right">
                <button class="toggle" onclick="myFunction()"><img src="{{ asset('user/images/cross-menu.png') }}" /></button>
            </div>
            <div class="logo-sec-menu">
                <div class="row">
                    <div class="col-lg-12">
                        <figure>
                            <a href="#"><img src="{{ asset('user/images/logo.png') }}" /></a>
                        </figure>
                    </div>
                </div>
            </div>
            <div class="nav-menu">
                <ul class="p-0 m-0">
                    @php
                        $count_others = 0;
                    @endphp
                    @foreach ($menus as $menu)
                        @if (!$menu->is_other_menu)
                            @if ($menu->link === 'dashboard' && $menu->status)
                                <li>
                                    <a href="{{ route('user.home') }}">
                                        <strong class="d-block">
                                            <span><img src="{{ asset('uploads/').'/'.$menu->icon }}" /></span> {{ $menu->title }}
                                        </strong>
                                    </a>
                                </li>
                            @endif
                            @if ($menu->link === 'levels' && $menu->status)
                                @foreach ($levels as $level)
                                    <li @if ($max_level->id < $level->id)class="disabled" @endif>
                                        <a href="{{ route('user.level', $level->id) }}">
                                            <span><img src="{{ asset('uploads/').'/'.$level->icon }}" /></span> {{ $level->title }}
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        @endif
                    @endforeach
                    @foreach ($menus as $menu)
                        @if ($menu->is_other_menu)
                            @if (!$count_others && $menu->status)
                                <li>
                                    <strong class="d-block"><span></span> RESOURCES</strong>
                                </li>
                                @php
                                    $count_others++;
                                @endphp
                            @endif
                            @if ($menu->status)
                                <li>
                                    @if ($menu->link === 'refer-people')
                                        <a href="{{ route('user.refer') }}">
                                            <span><img src="{{ asset('uploads/').'/'.$menu->icon }}" /></span> {{ $menu->title }}
                                        </a>
                                    @elseif ($menu->link === 'faq')
                                        <a href="{{ route('user.faq') }}">
                                            <span><img src="{{ asset('uploads/').'/'.$menu->icon }}" /></span> {{ $menu->title }}
                                        </a>
                                    @elseif ($menu->link === 'tool')
                                        <a href="{{ route('user.tool') }}">
                                            <span><img src="{{ asset('uploads/').'/'.$menu->icon }}" /></span> {{ $menu->title }}
                                        </a>
                                    @elseif ($menu->link === 'bonus-webinar')
                                        <a href="{{ $webinar_link->link }}" target="_blank">
                                            <span><img src="{{ asset('uploads/').'/'.$menu->icon }}" /></span> {{ $menu->title }}
                                        </a>
                                    @elseif ($menu->link === 'facebook-group')
                                        <a href="{{ $facebook_link->link }}" target="_blank">
                                            <span><img src="{{ asset('uploads/').'/'.$menu->icon }}" /></span> {{ $menu->title }}
                                        </a>
                                    @else
                                        @if ($menu->menu_type == 'page')
                                            <a href="{{ route('user.page', $menu->link) }}">
                                                <span><img src="{{ asset('uploads/').'/'.$menu->icon }}" /></span> {{ $menu->title }}
                                            </a>
                                        @elseif ($menu->menu_type == 'url')
                                            @php
                                                $link_content = DB::table('links')->where('menu_id', $menu->id)->first();
                                            @endphp
                                            <a @if ($link_content) href="{{ $link_content->link }}" @else href="javascript:void(0);" @endif target="_blank">
                                                <span><img src="{{ asset('uploads/').'/'.$menu->icon }}" /></span> {{ $menu->title }}
                                            </a>
                                        @endif
                                    @endif
                                </li>
                            @endif
                        @endif
                    @endforeach
                </ul>
            </div>
        </section>

        <section class="right-panel">
            <div class="container-fluid p-0">
                <div class="right-part-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="toggle-btn">
                                <button class="toggle" onclick="myFunction()"><img src="{{ asset('user/images/menu-icon.png') }}" /></button>
                            </div>

                            <div class="head-title">
                                <h2>{{ $max_level->title }}</h2>
                                <a href="{{ route('user.refer') }}">
                                    <span class="d-block">Refer To Upgrade</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <div class="user-icon">
                                <a href="" class="user-icon-wrapper"><img src="{{ asset('user/images/user-icon.png') }}" onclick="event.preventDefault(); toggleUserPopup();" /></a>
                                <div class="user-popup dashboard_usr_popup" style="display: none;">
                                    <div class="d-flex align-items-center">
                                        <figure class="mb-0">
                                            <img src="{{ asset('user/images/user-icon.png') }}" />
                                        </figure>
                                        <strong class="d-block">{{ $user->email }} <small class="d-block">{{ $user->total_referral }} Referrals Made</small></strong>
                                    </div>
                                    <ul>
                                        <li>
                                            <a href="{{ route('user.refer') }}">
                                                <span><img src="{{ asset('user/images/cup-icon.png') }}" /></span> Refer People
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('user.logout') }}" class="logout-btn" onclick="event.preventDefault(); getElementById('logout-form').submit();">Logout</a>
                                            <form action="{{ route('user.logout') }}" method="post" class="d-none" id="logout-form">@csrf</form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body-wrap">
                    @yield('content')
                </div>
            </div>
        </section>

        <script src="{{ asset('user/js/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('user/js/propper.js') }}"></script>
        <script src="{{ asset('user/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('user/js/custom.js') }}"></script>

        <script>
            function myFunction() {
                var element = document.getElementById("menu_target");
                element.classList.toggle("menu-toggle");
            }
            $(document).ready(function(){
                var panel_code = sessionStorage.setItem('panelValue', '');
            });

            function toggleUserPopup() {
                $('.dashboard_usr_popup').toggle();
            }
        </script>

        @stack('scripts')

    </body>
</html>
