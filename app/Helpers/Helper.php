<?php
namespace App\Helpers;

class Helper {

    public static function createReferralID() {
        return date('dHs');
    }

    public static function renameImageFile($path, $filename) {
        if ($pos = strrpos($filename, '.')) {
            $name = substr($filename, 0, $pos);
            $ext = substr($filename, $pos);
        } else {
            $name = $filename;
        }
        $newpath = $path.'/'.$filename;
        $newname = $filename;
        $counter = 0;
        while (file_exists($newpath)) {
            $newname = $name.'_'.$counter.$ext;
            $newpath = $path.'/'.$newname;
            $counter++;
        }
        return $newname;
    }

    public static function generateLink($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string)); // Removes special chars.
    }

    public static function generateNewOtp() {
        $six_digit_random_number = random_int(100000, 999999);
        return $six_digit_random_number;
    }
}

?>
