<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\FacebookContent;
use App\Models\DashboardContent;
use App\Models\LoginContent;
use App\Models\RegisterContent;
use App\Models\LevelContent;
use App\Models\ReferContent;
use App\Models\Level;
use App\Models\Menu;
use App\Models\Faq;
use App\Models\Page;
use App\Models\Tool;
use App\Models\Otp;
use App\Models\Notification;
use App\Models\Webhook;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;

class UserController extends Controller
{
    function login() {
        $data['content'] = LoginContent::find(1);
        return view('dashboard.user.login', $data);
    }

    function index() {
        $user = Auth::user();
        $data['max_level'] = Level::whereBetween('referrals', [0, $user->total_referral])->where('status', 'A')->orderByDesc('id')->first();
        $data['content'] = DashboardContent::find(1);
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['facebook_link'] = FacebookContent::find(1);
        $data['webinar_link'] = FacebookContent::find(2);
        $data['user'] = $user;
        return view('dashboard.user.home', $data);
    }

    function create(Request $request) {
        // validate form inputs
        $request->validate([
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:5|max:20',
            'full_name' => 'required',
            'phone' => 'required'
        ]);

        $referral_id = random_int(111111, 999999);

        $user_data = User::where('reffer_code', $request->panel_code)->first();
        $total_referral = ($user_data->total_referral + 1);

        $user = new User();
        $user->name = $request->full_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->brokerage = $request->brokerage;
        $user->entered_code = $request->panel_code;
        $user->password = \Hash::make($request->password);
        $save = $user->save();

        $referral_id = $result = substr($user->id.$referral_id, 0, 6);

        if($save) {
            $updating = User::where('id', $user->id)
                    ->update([
                        'reffer_code' => $referral_id
                    ]);
            $updating = User::where('id', $user_data->id)
                    ->update([
                        'total_referral' => $total_referral
                    ]);

            //webhook part start
            $final_array = array();
            $outgoing_webhooks = Webhook::where('type', 'outgoing')->get();
            foreach ($outgoing_webhooks as $key => $webhook) {
                if (strpos($webhook->trigger, 'user_add') !== false) {
                    $fields = explode(',', $webhook->fields);
                    foreach ($fields as $fkey => $value) {
                        if ($value == 'user_name') {
                            $final_array['name'] = $request->full_name;
                        }
                        if ($value == 'user_email') {
                            $final_array['email'] = $request->email;
                        }
                        if ($value == 'user_phone') {
                            $final_array['phone'] = $request->phone;
                        }
                        if ($value == 'user_brokerage') {
                            $final_array['brokerage'] = $request->brokerage;
                        }
                        if ($value == 'user_password') {
                            $final_array['password'] = $request->password;
                        }
                        if ($value == 'user_entered_code') {
                            $final_array['entered_code'] = $request->panel_code;
                        }
                        if ($value == 'user_refer_code') {
                            $final_array['refer_code'] = $referral_id;
                        }
                        if ($value == 'user_total_referral') {
                            $final_array['total_referral'] = 0;
                        }
                    }

                    $url = $webhook->webhook_url;
                    $hookData = [
                        'status_code' => 200,
                        'status' => 'success',
                        'message' => 'webhook send successfully',
                        'extra_data' => $final_array,
                    ];
                    $json_array = json_encode($hookData);
                    $curl = curl_init();
                    $headers = ['Content-Type: application/json'];

                    curl_setopt($curl, CURLOPT_URL, $url);
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_array);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HEADER, 1);
                    curl_setopt($curl, CURLOPT_TIMEOUT, 30);

                    $response = curl_exec($curl);
                    $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                    curl_close($curl);
                }
            }
            // end of webhook

            $creds = $request->only('email', 'password');
            if (Auth::guard('web')->attempt($creds)) {
                return redirect()->route('user.home');
            }
        } else {
            return redirect()->back()->with('fail', 'Something went wrong, failed to register');
        }
    }

    function check(Request $request) {
        // validate inputs
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:5|max:20'
        ],[
            'email.exists' => 'This email is not exists.'
        ]);

        $creds = $request->only('email', 'password');
        if (Auth::guard('web')->attempt($creds)) {
            return redirect()->route('user.home');
        } else {
            return redirect()->route('user.login')->with('fail', 'Incorrect credentials');
        }
    }

    function logout() {
        Auth::guard('web')->logout();
        return redirect('/');
    }

    function level($level_id) {
        $user = Auth::user();
        $data['max_level'] = Level::whereBetween('referrals', [0, $user->total_referral])->where('status', 'A')->orderByDesc('id')->first();
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['level_details'] = Level::find($level_id);
        $data['level_contents'] = LevelContent::where('level_id', $level_id)->get();
        $data['facebook_link'] = FacebookContent::find(1);
        $data['webinar_link'] = FacebookContent::find(2);
        $data['user'] = $user;
        return view('dashboard.user.level', $data);
    }

    function register() {
        $data['content'] = RegisterContent::find(1);
        $data['texts'] = RegisterContent::where('id', '!=', 1)->get();
        return view('dashboard.user.register', $data);
    }

    function refer() {
        $user = Auth::user();
        $data['max_level'] = Level::whereBetween('referrals', [0, $user->total_referral])->where('status', 'A')->orderByDesc('id')->first();
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['facebook_link'] = FacebookContent::find(1);
        $data['webinar_link'] = FacebookContent::find(2);
        $data['content'] = ReferContent::find(1);
        $data['reffer_code'] = $user->reffer_code;
        $data['user'] = $user;
        return view('dashboard.user.refer', $data);
    }

    function refer_check($code) {
        $user = User::where('reffer_code', $code)->first();
        if($user === null) {
            echo 'notfound';
        } else {
            echo 'found';
        }
    }

    function faq() {
        $user = Auth::user();
        $data['max_level'] = Level::whereBetween('referrals', [0, $user->total_referral])->where('status', 'A')->orderByDesc('id')->first();
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['facebook_link'] = FacebookContent::find(1);
        $data['webinar_link'] = FacebookContent::find(2);
        $data['faqs'] = Faq::all();
        $data['user'] = $user;
        return view('dashboard.user.faq', $data);
    }

    function page($id) {
        $user = Auth::user();
        $data['max_level'] = Level::whereBetween('referrals', [0, $user->total_referral])->where('status', 'A')->orderByDesc('id')->first();
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['facebook_link'] = FacebookContent::find(1);
        $data['webinar_link'] = FacebookContent::find(2);
        $data['user'] = $user;
        $menu = Menu::where('link', $id)->first();
        $data['page_content'] = Page::where('menu_id', $menu->id)->first();
        return view('dashboard.user.page', $data);
    }

    function tool() {
        $user = Auth::user();
        $data['max_level'] = Level::whereBetween('referrals', [0, $user->total_referral])->where('status', 'A')->orderByDesc('id')->first();
        $data['levels'] = Level::where('status', '!=', 'D')->get();
        $data['facebook_link'] = FacebookContent::find(1);
        $data['webinar_link'] = FacebookContent::find(2);
        $data['tools'] = Tool::all();
        $data['user'] = $user;
        return view('dashboard.user.tool', $data);
    }

    function check_email() {
        $email = $_GET['email'];
        $user = User::where('email', $email)->count();
        $notification = Notification::where('type', 'Forgot Password')->first();
        if ($user) {
            if ($_SERVER['SERVER_NAME'] == 'http://localhost/' || $_SERVER['SERVER_NAME'] == '127.0.0.1') {
                $new_otp = 123456;
            } else {
                $new_otp = Helper::generateNewOtp();
            }

            $data = new Otp();
            $data->type = 'Forgot Password';
            $data->email = $email;
            $data->otp = $new_otp;
            $data->status = 'active';
            $save = $data->save();
            if ($save) {
                $to = $email;
                $subject = $notification->subject;
                $message_body = str_replace("[forgot_password_otp]", $new_otp, $notification->body);

                $message = "
                <html>
                <head>
                <title>HTML email</title>
                </head>
                <body>
                ".$message_body."
                </body>
                </html>
                ";

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                // More headers
                $headers .= 'From: <donotreply@elite-agent.com>' . "\r\n";
                // $headers .= 'Cc: myboss@example.com' . "\r\n";

                if ($_SERVER['SERVER_NAME'] == 'http://localhost/' || $_SERVER['SERVER_NAME'] == '127.0.0.1') {

                } else {
                    mail($to,$subject,$message,$headers);
                }
                echo 'found';
            }
        } else {
            echo 'notfound';
        }
    }

    function check_otp() {
        $email = $_GET['email'];
        $otp = $_GET['otp'];
        $find_otp = Otp::where(['otp'=>$otp, 'email'=>$email])->orderByDesc('id')->first();
        if ($find_otp === null) {
            echo 'notfound';
        } else {
            if ($find_otp->status == 'expired') {
                echo 'expired';
            } else {
                $updating = Otp::where('id', $find_otp->id)
                    ->update([
                        'status' => 'expired'
                    ]);
                echo 'found';
            }
        }
    }

    function update_new_password() {
        $email = $_GET['email'];
        $new_password = \Hash::make($_GET['new_password']);
        $user = User::where('email', $email)->first();
        $updating = User::where('id', $user->id)
                    ->update([
                        'password' => $new_password
                    ]);

        //webhook part start
        $final_array = array();
        $outgoing_webhooks = Webhook::where('type', 'outgoing')->get();
        foreach ($outgoing_webhooks as $key => $webhook) {
            if (strpos($webhook->trigger, 'user_password_update') !== false) {
                $fields = explode(',', $webhook->fields);
                foreach ($fields as $fkey => $value) {
                    if ($value == 'user_name') {
                        $final_array['name'] = $user->name;
                    }
                    if ($value == 'user_email') {
                        $final_array['email'] = $user->email;
                    }
                    if ($value == 'user_phone') {
                        $final_array['phone'] = $user->phone;
                    }
                    if ($value == 'user_brokerage') {
                        $final_array['brokerage'] = $user->brokerage;
                    }
                    if ($value == 'user_password') {
                        $final_array['password'] = $_GET['new_password'];
                    }
                    if ($value == 'user_entered_code') {
                        $final_array['entered_code'] = $user->entered_code;
                    }
                    if ($value == 'user_refer_code') {
                        $final_array['refer_code'] = $user->reffer_code;
                    }
                    if ($value == 'user_total_referral') {
                        $final_array['total_referral'] = $user->total_referral;
                    }
                }

                $url = $webhook->webhook_url;
                $hookData = [
                    'status_code' => 200,
                    'status' => 'success',
                    'message' => 'webhook send successfully',
                    'extra_data' => $final_array,
                ];
                $json_array = json_encode($hookData);
                $curl = curl_init();
                $headers = ['Content-Type: application/json'];

                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $json_array);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_HEADER, 1);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);

                $response = curl_exec($curl);
                $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                curl_close($curl);
            }
        }
        // end of webhook
        echo 'complete';
    }
}
