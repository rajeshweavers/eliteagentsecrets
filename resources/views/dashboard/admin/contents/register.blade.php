@extends('dashboard.admin.layouts.master')
@section('title', 'Admin | Contents | Register')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Contents</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                <li class="breadcrumb-item active">Register Content</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                @if (Session::get('success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('success') }}
                </div>
                @endif
              <!-- jquery validation -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Register Content</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('admin.contents.register.save') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="text_one">Text One</label>
                            <textarea name="text_one" rows="4" class="form-control" id="text_one">{{ $content->text_one }}</textarea>
                            <span class="text-danger">@error('text_one'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="text_two">Text Two</label>
                            <textarea name="text_two" rows="4" class="form-control" id="text_two">{{ $content->text_two }}</textarea>
                            <span class="text-danger">@error('text_two'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="text_three">Text Three</label>
                            <textarea name="text_three" rows="4" class="form-control" id="text_three">{{ $content->text_three }}</textarea>
                            <span class="text-danger">@error('text_three'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="text_four">Text Four</label>
                            <textarea name="text_four" rows="4" class="form-control" id="text_four">{{ $content->text_four }}</textarea>
                            <span class="text-danger">@error('text_four'){{ $message }}@enderror</span>
                        </div>
                        <div class="form-group">
                            <label for="heading">Heading</label>
                            <input type="text" name="heading" class="form-control" id="heading" value="{{ $content->heading }}">
                            <span class="text-danger">@error('heading'){{ $message }}@enderror</span>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <a href="{{ route('admin.contents.register.add') }}" class="btn btn-dark" style="float:right;">Add Text</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Sl No.</th>
                            <th>Title</th>
                            <th>Text</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($texts as $text)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $text->title }}</td>
                                <td>{!! $text->text !!}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.contents.register.edit', $text->id) }}" class="text-primary"><i class="fas fa-edit"></i></a>
                                        <a href="{{ route('admin.contents.register.delete', $text->id) }}" class="text-danger"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.16.1/full/ckeditor.js"></script>
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
    CKEDITOR.replace('text_one', {
            filebrowserUploadUrl: "{{ route('admin.contents.faqs.upload', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('text_two', {
            filebrowserUploadUrl: "{{ route('admin.contents.faqs.upload', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('text_three', {
            filebrowserUploadUrl: "{{ route('admin.contents.faqs.upload', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('text_four', {
            filebrowserUploadUrl: "{{ route('admin.contents.faqs.upload', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form'
        });
</script>
@endpush
